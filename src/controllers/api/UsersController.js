const makeResponse = require('../../libraries/response');
const UserModel = require('../../models/UsersModel');

class UsersController {
    async index(req, res) {
        try {
            const model = UserModel();
            const data = await model.findAll(); // SELECT * FROM tb_car

            makeResponse(res, {
                code: 200,
                message: 'success fetch all users',
                data,
            });
        } catch (error) {
            console.log(error)
            makeResponse(res, {
                code: 500,
                message: 'failed fetch all users',
                data: {},
            })
        }
    }

    async create(req, res) {
        try {
            const {
                Username,
                Password,
                Email,
                FullName
            } = req.body;
            const userModel = UserModel();
            const data = await userModel.create({
                Username,
                Email,
                FullName,
                Password,
            });
            makeResponse(res, {
                code: 201,
                message: 'success create user',
                data,
            });
        } catch (error) {
            makeResponse(res, {
                code: 500,
                message: 'failed create user',
                data: error.toString(),
            });
        }
    }

    async update(req, res) {
        try {
            const ID = req.params.userID;
            const {
                Username,
                Password,
                Email,
                FullName
            } = req.body;
            const userModel = UserModel();
            const update = await userModel.update({
                Username,
                Email,
                Password,
                FullName,
            }, {
                where: {
                    ID,
                },
            });
            makeResponse(res, {
                code: 201,
                message: 'success update user',
                data: update,
            });
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }

    async remove(req, res) {
        try {
            const ID = req.params.userID;
            const userModel = UserModel();
            const response = userModel.destroy({
                where: {
                    ID,
                }
            });
            return res.status(200).json({
                data: response,
            });
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }

    async getByUserID(req, res) {
        try {
            const ID = req.params.userID;
            const userModel = UserModel();
            const data = await userModel.findOne({
                where: {
                    ID,
                }
            });
            makeResponse(res, {
                code: 200,
                message: 'success fetch users by ID',
                data,
            });
        } catch (error) {
            makeResponse(res, {
                code: 500,
                message: 'failed fetch all users',
                data,
            });
        }
    }
}

module.exports = new UsersController();